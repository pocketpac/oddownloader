/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package open.directory.oddownloader;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author tjhasty
 */
public class Main {

    public static void main(String[] args) {
        String Path = Main.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        File curFile = new File(Path);
        System.setProperty("java.net.useSystemProxies", "true");
        if (curFile.length() != 0L) {
            try {
                URL toGet = new URL("https://bitbucket.org/pocketpac/oddownloader/downloads/ODDownloader.jar");
                try {
                    long newSize = toGet.openConnection().getContentLengthLong();
                    if (newSize != -1) {
                        if (newSize != curFile.length()) {
                            int OK = JOptionPane.showConfirmDialog(null, "Update Detected, take you to the download page?");
                            if (OK == 0) {
                                Desktop.getDesktop().browse(new URL("https://bitbucket.org/pocketpac/oddownloader/downloads").toURI());
                                return;
                            }
                        }
                    }
                } catch (IOException | URISyntaxException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            } catch (MalformedURLException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        new Start().setVisible(true);
    }

}
