Instruction are Simple:
Go to [HERE](https://bitbucket.org/pocketpac/oddownloader/downloads) to download ODDownloader.jar.
Requires fully updated Java 7 or above.

Patch notes are the Commits. Go [HERE](https://bitbucket.org/pocketpac/oddownloader/commits/)

You can now queue up multiple parses before downloading. Put your Download location, Select if you want to search recursively, and put the URL to parse. Once you hit Parse it will store all values and clear the fields. Once parsing is complete you can move on to the next url to parse.

Once your are happy hit Download and away she goes.

Side note: A lot of Open Directories I have found get finicky if you do more than 1 download at a time, so if you have issues back it down to 1 and try again.